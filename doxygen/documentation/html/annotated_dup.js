var annotated_dup =
[
    [ "IL_INS", "d8/d5e/struct_i_l___i_n_s.html", "d8/d5e/struct_i_l___i_n_s" ],
    [ "IlMatrix3x3", "d3/df6/struct_il_matrix3x3.html", "d3/df6/struct_il_matrix3x3" ],
    [ "IlQuaternion", "d0/dd5/struct_il_quaternion.html", "d0/dd5/struct_il_quaternion" ],
    [ "IlVector3", "df/db8/struct_il_vector3.html", "df/db8/struct_il_vector3" ],
    [ "IlYpr", "da/df9/struct_il_ypr.html", "da/df9/struct_il_ypr" ],
    [ "INSCompositeData", "dc/d85/struct_i_n_s_composite_data.html", "dc/d85/struct_i_n_s_composite_data" ],
    [ "INSPositionData", "d9/d1b/struct_i_n_s_position_data.html", "d9/d1b/struct_i_n_s_position_data" ]
];