# Inertiallabs_ins_sdk

Cross-platform(Linux /Windows)SDK for the Inertial Labs INS.

[![alt text](https://readthedocs.org/projects/docs/badge/?version=latest "Documentation Status")](https://oblivione.gitlab.io/inertiallabs_ros_package/index.html)


![Picture of IMU](https://inertiallabs.com/static/assets/img/products/INS-D.jpg)

The `Inertiallabs_ins_sdk` is a Cross-platform(Linux /Windows) SDK for GPS-Aided Inertial Navigation Systems (INS) of [Inertial Labs](https://inertiallabs.com/). Currently the SDK is developed for Linux . The user manual for the device can be found [here](https://inertiallabs.com/static/pdf/INS-Datasheet.rev3.2_Nov_2018.pdf).

The SDK is tested on Ubuntu 16.04 LTS & 18.04 LTS .

## License

* The license for the official SDK is the MIT license which is included in the `ins_ros/inertiallabs_ins_sdk`
* The license for the other codes is Apache 2.0 whenever not specified.

## Compiling

```
$ git clone https://gitlab.com/oblivione/inertiallabs_ins_sdk
$ cd inertiallabs_ins_sdk/examples/<example_code_dir>
$ make clean
$ make

```

**Node**

Currently we provided one example code , how you can use SDK in your own code . You can find more details about the SDK functions [here](https://oblivione.gitlab.io/inertiallabs_ros_package/index.html)

to run the example code
```
 $ cd inertiallabs_ins_sdk/examples/ins_linux_basic
 $ make clean
 $ make
 $ ./ins_linux_basic

```


## FAQ

1. The driver can't open my device?\
Make sure you have ownership of the device in `/dev`.

2. Why I have permission error during the initialization process of the driver?\
Most often, this is because the baud rate you set does not match the package size to be received. Try increase the baud rate.

3. Why is the IMU data output rate much lower than what is set?\
This may be due to a recent change in the FTDI USB-Serial driver in the Linux kernel, the following shell script might help:
    ```bash
    # Reduce latency in the FTDI serial-USB kernel driver to 1ms
    # This is required due to https://github.com/torvalds/linux/commit/c6dce262
    for file in $(ls /sys/bus/usb-serial/devices/); do
      value=`cat /sys/bus/usb-serial/devices/$file/latency_timer`
      if [ $value -gt 1 ]; then
        echo "Setting low_latency mode for $file"
        sudo sh -c "echo 1 > /sys/bus/usb-serial/devices/$file/latency_timer"
      fi
    done
    ```

## Bug Report

Prefer to open an issue. You can also send an E-mail to omprakashpatro@gmail.com.

