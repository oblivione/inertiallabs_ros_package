/**
 * \file

 * \section DESCRIPTION
 * This header file provides access to features commonly used throughout the
 * entire InertialLabs C/C++ Library.
 */
#ifndef _IL_COMMON_H_
#define _IL_COMMON_H_

int IL_getErrorCode();

#endif /* _IL_COMMON_H_ */